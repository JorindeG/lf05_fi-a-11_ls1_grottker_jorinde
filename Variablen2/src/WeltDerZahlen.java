
public class WeltDerZahlen {

	public static void main(String[] args) {
		    
		    /*  *********************************************************
		    
		         Zuerst werden die Variablen mit den Werten festgelegt!
		    
		    *********************************************************** */
		    // Im Internet gefunden ?
		    // Die Anzahl der Planeten in unserem Sonnesystem                    
			  byte anzahlPlaneten =  8;
		    
		    // Anzahl der Sterne in unserer Milchstra�e
		      long  anzahlSterne = 400000000000l;
		    
		    // Wie viele Einwohner hat Berlin?
		      int  bewohnerBerlin = 3664000;
		    
		    // Wie alt bist du?  Wie viele Tage sind das?
		    
		      byte alter = 28;
		      int  alterTage = 28*365;
		    
		    // Wie viel wiegt das schwerste Tier der Welt?
		    // Schreiben Sie das Gewicht in Kilogramm auf!
		      int gewichtKilogramm =   200000;
		    
		    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
		      int flaecheGroessteLand = 17098242;
		    
		    // Wie gro� ist das kleinste Land der Erde?
		    
		      float flaecheKleinsteLand = 0.44f;
		    
		    
		    
		    
		    /*  *********************************************************
		    
		         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
		    
		    *********************************************************** */
		    
		    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
		    
		    System.out.println("Anzahl der Sterne: "+ anzahlSterne);
		    
		    System.out.println("Einwohner in Berlin: "+ bewohnerBerlin);
		    
		    System.out.println("Ich bin "+ alter + " Jahre alt");
		    
		    System.out.println("Das sind "+ alterTage + " Tage");
		    
		    System.out.println("Das gr��te Tier der Welt wiegt: "+ gewichtKilogramm + " kg");
		    
		    System.out.println("Das gr��te Land der Welt ist Russland: "+ flaecheGroessteLand + " qm");
		    
		    System.out.println("Das kleinste Land der Welt ist Vatikanstadt: "+ flaecheKleinsteLand + " qm\n");
		    
		 
		    
		    
		    System.out.println(" *******  Ende des Programms  ******* ");
		    
		 
		

	}

}

