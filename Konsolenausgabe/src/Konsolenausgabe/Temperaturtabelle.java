package Konsolenausgabe;

import java.util.Locale;

class Konsolenausgabe3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Aufgabe 3: Abgabe
	
		String tabelle = "|", fahrenheitwerte = "Fahrenheit", celsiuswerte = "Celsius",
				strich = "-----------------------";
				
		System.out.printf("%s %2s %9s\n", fahrenheitwerte, tabelle, celsiuswerte);
		System.out.printf("%s\n", strich);
		
		int i = 20, i1 = 10, i2 = 0, i3 = 20, i4 = 30;
		float t = 28.8889f, t1 = 23.3333f, t2 = 17.7778f, t3 = 6.6667f, t4 = 1.1111f;
		
		System.out.printf(Locale.US, "%+d %9s %9.2f\n", -i, tabelle, -t);
		System.out.printf(Locale.US, "%+d %9s %9.2f\n", -i1, tabelle, -t1);
		System.out.printf(Locale.US, "%+d %10s %9.2f\n", i2, tabelle, -t2);
		System.out.printf(Locale.US, "%+d %9s %9.2f\n", +i3, tabelle, -t3);
		System.out.printf(Locale.US, "%+d %9s %9.2f\n", +i4, tabelle, -t4);
		
	}

}
