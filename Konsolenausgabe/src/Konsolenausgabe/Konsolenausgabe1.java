package Konsolenausgabe;

class Konsolenausgabe1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Aufgabe 1a
		System.out.println("Das ist ein Beispielsatz." + " Ein Beispielsatz ist das.\n");
	
		// Aufgabe 1b
		System.out.println("Das ist ein \"Beispielsatz\".\n" + "Ein Beispielsatz ist das.\n");
		
		// Aufgabe 1c
	//Kommentar: der Befehl println() f�hrt einen Zeilenvorschub aus, ohne "ln" wird 
	//der Befehl in derselben Zeile ausgegeben 
		
		// Aufgabe 2
		
		String stern = "*************";
		System.out.printf("%7.1s\n", stern);
		System.out.printf("%8.3s\n", stern);
		System.out.printf("%9.5s\n", stern);
		System.out.printf("%10.7s\n", stern);
		System.out.printf("%11.9s\n", stern);
		System.out.printf("%12.11s\n", stern);
		System.out.printf("%13.13s\n", stern);
		System.out.printf("%8.3s\n", stern);
		System.out.printf("%8.3s\n", stern);

	}

}
